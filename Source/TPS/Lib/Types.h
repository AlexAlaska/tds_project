#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Normal UMETA(DisplayName = "Normal"),
	Walk UMETA(DisplayName = "Walk"),
	Run UMETA(DisplayName = "Run"),
	AimNormal UMETA(DisplayName = "Aim Normal"),
	AimWalk UMETA(DisplayName = "Aim Walk")
};

USTRUCT(BlueprintType)
struct FCharacterCameraSettings
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		float CameraHeight = 5000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		float CameraFOV = 30.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		FRotator CameraRotation = FRotator(-60.0f, 0.0f, 0.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		float CameraZoomSpeed = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		float CameraInterpZoomSpeed = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		float CameraMaxZoom = CameraHeight * 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		float CameraMinZoom = CameraHeight * 0.25f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		bool CameraFlow = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		float CameraLagSpeed = 5.0f;
};

USTRUCT(BlueprintType)
struct FCharacterMovementSettings
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		float NormalSpeed = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		float WalkSpeed = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		float AimNormalSpeed = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		float AimWalkSpeed = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		float CharacterYawSpeed = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		float CharacterBodyYawSpeed = 8.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		FRotator CharacterRotationRate = FRotator(0.0f, 320.0f, 0.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		float StaminaMax = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		float StaminaDecrease = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		float StaminaIncrease = 100.0f;
};

UCLASS()
class TPS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};


