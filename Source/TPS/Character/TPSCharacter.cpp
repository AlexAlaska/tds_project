#include "TPS/Character/TPSCharacter.h"
#include "TPS/Character/TPSAnimInstance.h"

#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"

ATPSCharacter::ATPSCharacter()
{	
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.0f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	CharacterMaxWalkSpeedPtr = &GetCharacterMovement()->MaxWalkSpeed;

	// Create a camera boom
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	CameraCurrentArmLengthPtr = &CameraBoom->TargetArmLength;
	
	// Create a camera
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	
	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);
	PlayerInputComponent->BindAxis(TEXT("MouseWheel"), this, &ATPSCharacter::CameraZoom);
	PlayerInputComponent->BindAction(TEXT("Aim"), IE_Pressed, this, &ATPSCharacter::AimOn);
	PlayerInputComponent->BindAction(TEXT("Aim"), IE_Released, this, &ATPSCharacter::AimOff);
	PlayerInputComponent->BindAction(TEXT("Run"), IE_Pressed, this, &ATPSCharacter::RunOn);
	PlayerInputComponent->BindAction(TEXT("Run"), IE_Released, this, &ATPSCharacter::RunOff);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Pressed, this, &ATPSCharacter::WalkOn);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &ATPSCharacter::WalkOff);
}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	PlayerController = Cast<APlayerController>(GetController());
	if(!IsValid(PlayerController))
		UE_LOG(LogTemp, Error, TEXT("%s: Error init player controller"), *GetName());

	CameraBoom->SetRelativeRotation(CameraSettings.CameraRotation);
	CameraBoom->bEnableCameraLag = CameraSettings.CameraFlow;
	CameraBoom->CameraLagSpeed = CameraSettings.CameraLagSpeed;
	*CameraCurrentArmLengthPtr = CameraSettings.CameraHeight;
	CameraTargetArmLength = *CameraCurrentArmLengthPtr;
	TopDownCameraComponent->SetFieldOfView(CameraSettings.CameraFOV);
	GetCharacterMovement()->RotationRate = MovementSettings.CharacterRotationRate;
	ChangeMovementState(EMovementState::Normal);
	Stamina = MovementSettings.StaminaMax;
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		FHitResult HitResult;
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorHitLocation = HitResult.Location;
				CursorToWorld->SetWorldLocationAndRotation(CursorHitLocation, SurfaceRotation);
			}
		}
		else 
		{
			PlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel6, true, HitResult);
//			PlayerController->GetHitResultUnderCursorByChannel(TraceTypeQuery6, true, HitResult);
			FVector CursorFV = HitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorHitLocation = HitResult.Location;
			CursorToWorld->SetWorldLocation(CursorHitLocation);
			CursorToWorld->SetWorldRotation(CursorR);
		}
		
		if((int)*CameraCurrentArmLengthPtr != (int)CameraTargetArmLength)
			*CameraCurrentArmLengthPtr = FMath::FInterpTo(*CameraCurrentArmLengthPtr, CameraTargetArmLength, DeltaSeconds, CameraSettings.CameraInterpZoomSpeed);
	
		MovementTick(DeltaSeconds);
	}
}

void ATPSCharacter::MovementTick(float DeltaSeconds)
{
	CharacterRotation = GetActorRotation();
	CharToCursorVector = CursorHitLocation - GetActorLocation();
	CharacterVelocity = GetVelocity();
	
	if(AxisX || AxisY)
	{
		if(bIsRun)
		{
			AddMovementInput(CharToCursorVector, 1.0f);
			RotateToCursor(DeltaSeconds);
			
			if(*CharacterMaxWalkSpeedPtr > MovementSettings.NormalSpeed && Stamina > 0.0f)
			{
				Stamina -= MovementSettings.StaminaDecrease * DeltaSeconds; 
				*CharacterMaxWalkSpeedPtr = MovementSettings.NormalSpeed + Stamina;
			}
			else
			{
				bIsRun = false;
			}
		}
		else
		{
			AddMovementInput(FVector::ForwardVector, AxisX);
			AddMovementInput(FVector::RightVector, AxisY);
		}
	}
	
	if(bIsAim)
	{
		RotateToCursor(DeltaSeconds);
	}
	
	if(!bIsRun && Stamina < MovementSettings.StaminaMax)
	{
		Stamina +=  MovementSettings.StaminaIncrease * DeltaSeconds;
	}
}

void ATPSCharacter::RotateToCursor(float DeltaSeconds)
{
	FRotator TargetRotation = UKismetMathLibrary::MakeRotFromX(CharToCursorVector);
		
	float Diff = TargetRotation.Yaw - CharacterRotation.Yaw;
	if (Diff < -180.0f)
		TargetRotation.Yaw += 360.0f;
	else if (Diff > 180.0f)
		TargetRotation.Yaw -= 360.0f;
		
	CharacterRotation.Yaw = FMath::FInterpTo(CharacterRotation.Yaw, TargetRotation.Yaw, DeltaSeconds, MovementSettings.CharacterYawSpeed);
	SetActorRotation(CharacterRotation);
}

void ATPSCharacter::CameraZoom(float AxisValue)
{
	if(AxisValue != 0.0f)
	{
		float TargetZoom = *CameraCurrentArmLengthPtr - AxisValue * CameraSettings.CameraZoomSpeed;
		
		if(TargetZoom > CameraSettings.CameraMaxZoom)
			TargetZoom = CameraSettings.CameraMaxZoom;
		else if (TargetZoom < CameraSettings.CameraMinZoom)
			TargetZoom = CameraSettings.CameraMinZoom;
		
		CameraTargetArmLength = TargetZoom;
	}
}

void ATPSCharacter::WalkOn()
{
	if(!bIsRun)
	{
		bIsWalk = true;
		if(bIsAim)
		{
			ChangeMovementState(EMovementState::AimWalk);
		}
		else
		{
			ChangeMovementState(EMovementState::Walk);
		}
	}
}

void ATPSCharacter::WalkOff()
{
	if(bIsWalk)
	{
		bIsWalk = false;
		if(bIsAim)
		{
			ChangeMovementState(EMovementState::AimNormal);
		}
		else
		{
			ChangeMovementState(EMovementState::Normal);
		}
	}
}

void ATPSCharacter::RunOn()
{
	if(!bIsWalk)
	{
		bIsRun = true;
		bIsAim = false;
		ChangeMovementState(EMovementState::Run);
	}
}

void ATPSCharacter::RunOff()
{
	bIsRun = false;
	if(!bIsWalk && !bIsAim)
		ChangeMovementState(EMovementState::Normal);
}

void ATPSCharacter::AimOn()
{
	bIsAim = true;
	bIsRun = false;
	GetCharacterMovement()->bOrientRotationToMovement = false;
	if(bIsWalk)
	{
		ChangeMovementState(EMovementState::AimWalk);
	}
	else
	{
		ChangeMovementState(EMovementState::AimNormal);
	}
}

void ATPSCharacter::AimOff()
{
	if(bIsAim)
	{
		bIsAim = false;
		GetCharacterMovement()->bOrientRotationToMovement = true;
		if(bIsWalk)
		{
			ChangeMovementState(EMovementState::Walk);
		}
		else if(bIsRun)
		{
			ChangeMovementState(EMovementState::Run);
		}
		else
		{
			ChangeMovementState(EMovementState::Normal);
		}
	}
}

void ATPSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	float ResultSpeed = MovementSettings.NormalSpeed;
	switch(NewMovementState)
	{
		case EMovementState::Normal:
		CurrentMovementState = EMovementState::Normal;
		break;
		
		case EMovementState::Walk:
		CurrentMovementState = EMovementState::Walk;
		ResultSpeed = MovementSettings.WalkSpeed;
		break;
		
		case EMovementState::Run:
		CurrentMovementState = EMovementState::Run;
		ResultSpeed = MovementSettings.NormalSpeed + Stamina;
		break;
		
		case EMovementState::AimNormal:
		CurrentMovementState = EMovementState::AimNormal;
		ResultSpeed = MovementSettings.AimNormalSpeed;
		break;
		
		case EMovementState::AimWalk:
		CurrentMovementState = EMovementState::AimWalk;
		ResultSpeed = MovementSettings.AimWalkSpeed;
		break;
	}
	*CharacterMaxWalkSpeedPtr = ResultSpeed;
}
