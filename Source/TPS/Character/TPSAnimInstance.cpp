#include "TPS/Character/TPSAnimInstance.h"
#include "TPS/Character/TPSCharacter.h"
#include "Math/UnrealMathUtility.h"

void UTPSAnimInstance::NativeBeginPlay()
{
	CharacterOwner = Cast<ATPSCharacter>(TryGetPawnOwner());
	if(!IsValid(CharacterOwner))
		UE_LOG(LogTemp, Error, TEXT("%s: Error init pawn owner"), *GetName());
}

void UTPSAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if(CharacterOwner)
	{
		CharacterIsAim = CharacterOwner->bIsAim;
		CharacterSpeed = CharacterOwner->CharacterVelocity.Size();
		
		if(CharacterSpeed > 0.0f)
		{
			CharacterDirection = CalculateDirection(CharacterOwner->CharacterVelocity, CharacterOwner->CharacterRotation);
		}
		else
		{
			float TargetDirection = CalculateDirection(CharacterOwner->CharToCursorVector, CharacterOwner->CharacterRotation);
			if (TargetDirection < -120.0f || TargetDirection > 120.0f)
			{
				CharacterOwner->RotateToCursor(DeltaSeconds);
			}
			else
			{
				CharacterDirection = FMath::FInterpTo(CharacterDirection, TargetDirection, DeltaSeconds, CharacterOwner->MovementSettings.CharacterBodyYawSpeed);
			}
		}
	}
}
