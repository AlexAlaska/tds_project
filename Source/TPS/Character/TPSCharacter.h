#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS/Lib/Types.h"
#include "TPSCharacter.generated.h"

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter
{
	GENERATED_BODY()
	
	friend class UTPSAnimInstance;
	
public:
	// VARIABLES
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Development")
		FCharacterMovementSettings MovementSettings;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Development")
		FCharacterCameraSettings CameraSettings;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Development")
		EMovementState CurrentMovementState;
		
	// FUNCTIONS
	ATPSCharacter();
	
	/* Returns TopDownCameraComponent subobject */
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/* Returns CameraBoom subobject */
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/* Returns CursorToWorld subobject */
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NewMovementState);
	
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	// VARIABLES
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Development", meta = (AllowPrivateAccess = "true"))
		float Stamina;
	
	float AxisX = 0.0f;
	float AxisY = 0.0f;
	
	bool bIsWalk = false;
	bool bIsRun = false;
	bool bIsAim = false;
	
	FRotator CharacterRotation;
	FVector CursorHitLocation;
	FVector CharToCursorVector;
	FVector CharacterVelocity;
	
	/* Pointer to CharacterMovementComponent->TargetArmLength */
	float* CameraCurrentArmLengthPtr;
	float CameraTargetArmLength;
	/* Pointer to CharacterMovementComponent->MaxWalkSpeed */
	float* CharacterMaxWalkSpeedPtr;
	
	/* Player Controller */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class APlayerController* PlayerController;
	/* Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;
	/* Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;
	/* A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UDecalComponent* CursorToWorld;
	
	// FUNCTIONS
	FORCEINLINE void InputAxisX(float AxisValue) { AxisX = AxisValue; }
	FORCEINLINE void InputAxisY(float AxisValue) { AxisY = AxisValue; }
	
	virtual void BeginPlay() override;
	void MovementTick(float DeltaSeconds);
	void RotateToCursor(float DeltaSeconds);
	void CameraZoom(float AxisValue);
	void WalkOn();
	void WalkOff();
	void RunOn();
	void RunOff();
	void AimOn();
	void AimOff();
};
