#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "TPSAnimInstance.generated.h"

UCLASS()
class TPS_API UTPSAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category ="Character Owner", meta = (AllowPrivateAccess = "true"))
		float CharacterSpeed = 0.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category ="Character Owner", meta = (AllowPrivateAccess = "true"))
		float CharacterDirection = 0.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category ="Character Owner", meta = (AllowPrivateAccess = "true"))
		bool CharacterIsAim = false;

	class ATPSCharacter* CharacterOwner;
	
	virtual void NativeBeginPlay() override;
};
